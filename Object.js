//////////////////////////////Object.create
Object.prototype.create = function(objPrototype, props) {
		
		if (typeof objPrototype !== 'object') {
			throw TypeError ('Proto must be object or null')
		};

		let newObject = {};
		newObject.__proto__ = objPrototype;
		
		if (props && typeof props === 'object') {
			for (let prop in props) {
				if (props.hasOwnProperty(prop)) {
					Object.defineProperty(newObject, prop, props[prop])
				};
			};
		};
		return newObject;
	};
//////////////////////////////Object.keys
Object.keys = function(obj) {
	if (typeof obj !== 'object'){
		throw TypeError(`${obj} is not an object!`)
	}

	let keysObj = [];
	for (let prop in obj){
		if (obj.hasOwnProperty(prop)) {
			keysObj.push(prop);
		};
	};
	return keysObj;
};
//////////////////////////////Object.freeze
Object.prototype.freeze = function(obj){
	if (typeof obj === null || typeof obj === 'function'){
		throw TypeError(`${obj} is a Function or null`);
	};

	if (typeof obj !== 'object') {
		return obj;
	};

	for (let prop in obj){
		if (obj.hasOwnProperty(prop)) {
			let descriptor = Object.getOwnPropertyDescriptor(obj, prop);	
			descriptor.writable = false;
			descriptor.configurable = false;
			Object.defineProperty(obj, prop, descriptor);
		};
	};

	return Object.preventExtensions(obj);
};