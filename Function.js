////////////CALL/////////////////////
Function.prototype.call = function(context){
	if (typeof this !== 'function'){
		throw TypeError(`${this} is not a function`);
	};

	context = context || global;
	let tempProp = 'aa' + Math.random();
	
	while (context.hasOwnProperty(tempProp)) {
		tempProp = 'aa' + Math.random();
	};

	context[tempProp] = this;

	let args = [];
	for (let i = 1, i < arguments.length, i++){
		args.push('arguments['+ i + ']');
	}
	let output = eval('context[tempProp](' + args + ')');
	delete context[tempProp];
	return output;
}
////////////APPLY/////////////////////
Function.prototype.apply = function(context, arrArgs){
	if (typeof this !== 'function'){
		throw TypeError(`${this} is not a function`);
	};

	context = context || global;
	let tempProp = 'aa' + Math.random();
	
	while (context.hasOwnProperty(tempProp)) {
		tempProp = 'aa' + Math.random();
	};

	context[tempProp] = this;

	let args = [];
	let output = null;
	
	if (!arrArgs) {
		output = context[tempProp];
	} else {
		for (let i = 1, i < arguments.length, i++){
			args.push('arguments['+ i + ']');
		};
		output = eval('context[tempProp](' + args + ')');
	};

	delete context[tempProp];
	return output;
};
////////////BIND/////////////////////
Function.prototype.bind = function(context){
	if (typeof this !== 'function'){
		throw TypeError(`${this} is not a function`);
	};
	
	let bindArgs = Array.prototype.slice.call(arguments, 1);
	let bindingFunc = this;
	let tempFunc = function(){};
  let bindedFunc = function(){
		return bindingFunc.apply(this instanceof tempFunc
						? this
						: context,
						bindArgs.concat(Array.prototype.slice.call(arguments)));  
  }

  if (this.prototype) {
  	tempFunc.prototype = this.prototype;
  };
  bindedFunc.prototype = new tempFunc();

  return bindedFunc;
};