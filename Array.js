/////////POP///////////////////
Array.prototype.pop = function(){
	if (this.length === 0){
		return undefined;
	};
	let popped = this[this.length - 1];
	--this.length;
	return popped; 
};
/////////PUSH///////////////////
Array.prototype.push = function(){
	let j = this.length;
	for (let i = 0; i < arguments.length; i++){
		this[j + i] = arguments[i];
	};
};
/////////SHIFT///////////////////
Array.prototype.shift = function(){
	if (this.length === 0){
		return undefined;
	};
	let shifted = this[0];
	for (let i = 1; i < this.length; i++){
		this[i - 1] = this[i];
	};
	--this.length;
	return shifted;
};
/////////UNSHIFT///////////////////
Array.prototype.unshift = function(){
	arrLength = this.length
	for (let i = 0; i < arrLength; i++){
		this[i + arguments.length] = this[i];
	};
	for (let i = 0; i < arguments.length; i++){
		this[i] = arguments[i];
	};
};
/////////MAP///////////////////
Array.prototype.map = function(cbFunc, thisCb){
	if (typeof cbFunc !== 'function') {
		throw new TypeError(`${cbFunc} is not a function`)
	};
	
	let result = new Array(this.length);
	
	for (let i = 0; i < this.length; i++){
		if (this[i] == null) {continue};
		result[i] = cbFunc.call(thisCb, this[i], i, this);
	};

	return result;
};
/////////FOREACH///////////////////
Array.prototype.forEach = function(cbFunc, thisCb){
	if (typeof cbFunc !== 'function') {
		throw new TypeError(`${cbFunc} is not a function`)
	};

	for (let i = 0; i < this.length; i++){
		if (this[i] == null) {continue};
		cbFunc.call(thisCb, this[i], i, this);
	};
};
/////////FILTER///////////////////
Array.prototype.filter = function(cbFunc, thisCb){
	if (typeof cbFunc !== 'function') {
		throw new TypeError(`${cbFunc} is not a function`)
	};

	let results = [];
	for (let i = 0; i < this.length; i++){
		if (cbFunc.call(thisCb, this[i], i, this)) {
			results.push(this[i]);
		};
	};
	return results;
};
/////////REVERSE///////////////////
Array.prototype.reverse = function(){
	let k, l = this.length;
		
	if (l % 2 === 0) {
		k = l/2;
	} else {
		k = l/2 - 1;
	};
	
	for (let i = 0; i < k; i++){
		let tempElem = this[i];
		this[i] = this[l - i - 1];
		this[l - i - 1] = tempElem;
	};		
};
/////////JOIN///////////////////
Array.prototype.join = function(str = ','){
	if (this.length == 0) {
		return '';
	};

	let output = '' + this[0];
	for (let i = 1; i < this.length; i++){
		output += str + this[i];
	};

	return output;
};
/////////REDUCE///////////////////
Array.prototype.reduce = function(cbFunc, initValue){
	if (typeof cbFunc !== 'function') {
		throw new TypeError(`${cbFunc} is not a function`)
	};
	
	if (arguments.length > 1) {
		initValue = arguments[1];
	} else {
		if (this.length < 2) {
			throw new TypeError('Reduce can\'t be applied to this object');
		} else {
			initValue = this.shift();
		};
	};

	for (let i = 0; i < this.length; i++){
		initValue = cbFunc(initValue, this[i], i, this);
	};

	return initValue;
};
/////////SORT///////////////////
Array.prototype.sort = function(cbFunc){
	if (!cbFunc) {
		cbFunc = function(a, b){
			if ('' + a > '' + b) {
				return 1;
			} else {
				if ('' + a < '' + b) {
					return -1;
				} else {
					return 0;
				};
			};
		};
	};
	
	for (let i = 0; i < this.length - 1; i++){
		for (let j = 0; j < this.length - i - 1; j++){
			if (cbFunc(this[j],this[j + 1]) > 0) {
				let tmp = this[j];
				this[j] = this[j + 1];
				this[j + 1] = tmp;
			};	
		};
	};
};
/////////SOME///////////////////
Array.prototype.some = function(cbFunc, thisCb){
	if (typeof cbFunc !== 'function') {
		throw new TypeError(`${cbFunc} is not a function`)
	};

	for (let i = 0; i < this.length; i++){
		if (this[i] == null) {continue};
		if (cbFunc.call(thisCb, this[i], i, this))
			return true;
	};
	return false;
};
/////////EVERY///////////////////
Array.prototype.every = function(cbFunc, thisCb){
	if (typeof cbFunc !== 'function') {
		throw new TypeError(`${cbFunc} is not a function`)
	};

	for (let i = 0; i < this.length; i++){
		if (this[i] == null) {continue};
		if (!cbFunc.call(thisCb, this[i], i, this))
			return false;
	};
	return true;
};